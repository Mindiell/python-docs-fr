# Copyright (C) 2001-2018, Python Software Foundation
# For licence information, see README file.
#
msgid ""
msgstr ""
"Project-Id-Version: Python 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-15 22:33+0100\n"
"PO-Revision-Date: 2022-02-11 13:53+0100\n"
"Last-Translator: Arnaud Fréalle <arnaud.frealle@gmail.com>\n"
"Language-Team: FRENCH <traductions@lists.afpy.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0\n"

#: library/glob.rst:2
msgid ":mod:`glob` --- Unix style pathname pattern expansion"
msgstr ":mod:`glob` — Recherche de chemins de style Unix selon certains motifs"

#: library/glob.rst:7
msgid "**Source code:** :source:`Lib/glob.py`"
msgstr "**Code source :** :source:`Lib/glob.py`"

#: library/glob.rst:21
#, fuzzy
msgid ""
"The :mod:`glob` module finds all the pathnames matching a specified pattern "
"according to the rules used by the Unix shell, although results are returned "
"in arbitrary order.  No tilde expansion is done, but ``*``, ``?``, and "
"character ranges expressed with ``[]`` will be correctly matched.  This is "
"done by using the :func:`os.scandir` and :func:`fnmatch.fnmatch` functions "
"in concert, and not by actually invoking a subshell."
msgstr ""
"Le module :mod:`glob` recherche tous les chemins correspondant à un motif "
"particulier selon les règles utilisées par le shell Unix, les résultats sont "
"renvoyés dans un ordre arbitraire. Aucun remplacement du tilde n'est "
"réalisé, mais les caractères ``*``, ``?``, et les caractères ``[]`` "
"exprimant un intervalle sont correctement renvoyés. Cette opération est "
"réalisée en utilisant les fonctions :func:`os.scandir` et :func:`fnmatch."
"fnmatch` de concert, et sans invoquer une sous-commande. Notons qu'à la "
"différence de :func:`fnmatch.fnmatch`, :mod:`glob` traite les noms de "
"fichiers commençant par un point (``.``) comme des cas spéciaux. (Pour "
"remplacer le tilde et les variables shell, nous vous conseillons d'utiliser "
"les fonctions :func:`os.path.expanduser` et :func:`os.path.expandvars`.)"

#: library/glob.rst:28
msgid ""
"Note that files beginning with a dot (``.``) can only be matched by patterns "
"that also start with a dot, unlike :func:`fnmatch.fnmatch` or :func:`pathlib."
"Path.glob`. (For tilde and shell variable expansion, use :func:`os.path."
"expanduser` and :func:`os.path.expandvars`.)"
msgstr ""

#: library/glob.rst:34
msgid ""
"For a literal match, wrap the meta-characters in brackets. For example, "
"``'[?]'`` matches the character ``'?'``."
msgstr ""
"Pour une correspondance littérale, il faut entourer le métacaractère par des "
"crochets. Par exemple, ``'[?]'`` reconnaît le caractère ``'?'``."

#: library/glob.rst:39
msgid "The :mod:`pathlib` module offers high-level path objects."
msgstr ""
"Le module :mod:`pathlib` offre une représentation objet de haut niveau des "
"chemins."

#: library/glob.rst:45
#, fuzzy
msgid ""
"Return a possibly empty list of path names that match *pathname*, which must "
"be a string containing a path specification. *pathname* can be either "
"absolute (like :file:`/usr/src/Python-1.5/Makefile`) or relative (like :file:"
"`../../Tools/\\*/\\*.gif`), and can contain shell-style wildcards. Broken "
"symlinks are included in the results (as in the shell). Whether or not the "
"results are sorted depends on the file system.  If a file that satisfies "
"conditions is removed or added during the call of this function, whether a "
"path name for that file be included is unspecified."
msgstr ""
"Renvoie une liste, potentiellement vide, de chemins correspondant au motif "
"*pathname*, qui doit être une chaîne de caractères contenant la "
"spécification du chemin. *pathname* peut être soit absolu (comme :file:`/usr/"
"src/Python-1.5/Makefile`) soit relatif (comme :file:`../../Tools/\\*/\\*."
"gif`), et contenir un caractère de remplacement de style shell. Les liens "
"symboliques cassés sont aussi inclus dans les résultats (comme pour le "
"shell). Le fait que les résultats soient triés ou non dépend du système de "
"fichiers."

#: library/glob.rst:54
msgid ""
"If *root_dir* is not ``None``, it should be a :term:`path-like object` "
"specifying the root directory for searching.  It has the same effect on :"
"func:`glob` as changing the current directory before calling it.  If "
"*pathname* is relative, the result will contain paths relative to *root_dir*."
msgstr ""
"Si *root_dir* n'est pas ``None``, cela doit être un :term:`objet simili-"
"chemin <path-like object>` spécifiant le dossier racine de la recherche.  "
"Cela a le même effet sur :func:`glob` que de changer le dossier courant "
"avant l'appel de la fonction.  Si *pathname* est relatif, les chemins du "
"résultat seront relatifs au *root_dir* ."

#: library/glob.rst:60
msgid ""
"This function can support :ref:`paths relative to directory descriptors "
"<dir_fd>` with the *dir_fd* parameter."
msgstr ""
"Cette fonction prend en charge les :ref:`chemins relatifs aux descripteurs "
"de dossier <dir_fd>` avec le paramètre *dir_fd*."

#: library/glob.rst:66
msgid ""
"If *recursive* is true, the pattern \"``**``\" will match any files and zero "
"or more directories, subdirectories and symbolic links to directories. If "
"the pattern is followed by an :data:`os.sep` or :data:`os.altsep` then files "
"will not match."
msgstr ""
"Si *recursive* est vrai, le motif \"``**``\" reconnaît tous les fichiers, "
"aucun ou plusieurs répertoires, sous-répertoires et liens symboliques aux "
"répertoires. Si le motif est suivi par :data:`os.sep` ou :data:`os.altsep` "
"alors les fichiers ne sont pas inclus dans le résultat."

#: library/glob.rst:71
msgid ""
"If *include_hidden* is true, \"``**``\" pattern will match hidden "
"directories."
msgstr ""

#: library/glob.rst:96
msgid ""
"Raises an :ref:`auditing event <auditing>` ``glob.glob`` with arguments "
"``pathname``, ``recursive``."
msgstr ""
"Lève un :ref:`évènement d'audit <auditing>` ``glob.glob`` avec les arguments "
"``pathname``, ``recursive``."

#: library/glob.rst:97
msgid ""
"Raises an :ref:`auditing event <auditing>` ``glob.glob/2`` with arguments "
"``pathname``, ``recursive``, ``root_dir``, ``dir_fd``."
msgstr ""
"Lève un :ref:`évènement d'audit <auditing>` ``glob.glob/2`` avec les "
"arguments ``pathname``, ``recursive``, ``root_dir``, ``dir_fd``."

#: library/glob.rst:77
msgid ""
"Using the \"``**``\" pattern in large directory trees may consume an "
"inordinate amount of time."
msgstr ""
"Utiliser le motif \"``**``\" dans de grandes arborescences de dossier peut "
"consommer une quantité de temps démesurée."

#: library/glob.rst:99
msgid "Support for recursive globs using \"``**``\"."
msgstr "Prise en charge des chemins récursifs utilisant le motif  \"``**``\"."

#: library/glob.rst:102
msgid "Added the *root_dir* and *dir_fd* parameters."
msgstr "Paramètres *root_dir* et *dir_fd* ajoutés."

#: library/glob.rst:105
msgid "Added the *include_hidden* parameter."
msgstr ""

#: library/glob.rst:93
msgid ""
"Return an :term:`iterator` which yields the same values as :func:`glob` "
"without actually storing them all simultaneously."
msgstr ""
"Renvoie un :term:`itérateur <iterator>` qui produit les mêmes valeurs que :"
"func:`glob`, sans toutes les charger en mémoire simultanément."

#: library/glob.rst:111
msgid ""
"Escape all special characters (``'?'``, ``'*'`` and ``'['``). This is useful "
"if you want to match an arbitrary literal string that may have special "
"characters in it.  Special characters in drive/UNC sharepoints are not "
"escaped, e.g. on Windows ``escape('//?/c:/Quo vadis?.txt')`` returns ``'//?/"
"c:/Quo vadis[?].txt'``."
msgstr ""
"Échappe tous les caractères spéciaux (``'?'``, ``'*'`` et ``'['``). Cela est "
"utile pour reconnaître une chaîne de caractère littérale arbitraire qui "
"contiendrait ce type de caractères. Les caractères spéciaux dans les disques "
"et répertoires partagés (chemins UNC) ne sont pas échappés, e.g. sous "
"Windows ``escape('//?/c:/Quo vadis?.txt')`` renvoie ``'//?/c:/Quo vadis[?]."
"txt'``."

#: library/glob.rst:120
msgid ""
"For example, consider a directory containing the following files: :file:`1."
"gif`, :file:`2.txt`, :file:`card.gif` and a subdirectory :file:`sub` which "
"contains only the file :file:`3.txt`.  :func:`glob` will produce the "
"following results.  Notice how any leading components of the path are "
"preserved. ::"
msgstr ""
"Par exemple, considérons un répertoire contenant les fichiers suivants : :"
"file:`1.gif`, :file:`2.txt`, :file:`card.gif` et un sous-répertoire :file:"
"`sub` contenant seulement le fichier :file:`3.txt`. :func:`glob`  produit "
"les résultats suivants. Notons que les composantes principales des chemins "
"sont préservées. ::"

#: library/glob.rst:138
msgid ""
"If the directory contains files starting with ``.`` they won't be matched by "
"default. For example, consider a directory containing :file:`card.gif` and :"
"file:`.card.gif`::"
msgstr ""
"Si le répertoire contient des fichiers commençant par ``.``, ils ne sont pas "
"reconnus par défaut. Par exemple, considérons un répertoire contenant :file:"
"`card.gif` et :file:`.card.gif` ::"

#: library/glob.rst:150
msgid "Module :mod:`fnmatch`"
msgstr "Module :mod:`fnmatch`"

#: library/glob.rst:151
msgid "Shell-style filename (not path) expansion"
msgstr ""
"Recherche de noms de fichiers de style shell (ne concerne pas les chemins)"
